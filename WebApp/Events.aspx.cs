﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Model;

namespace WebApp
{
    public partial class Events : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Repository repo = new Repository();
            List<Event> events = repo.GetEvents(true);
            eventGridView.DataSource = events;
            eventGridView.DataBind();  
        }

        protected void CreateButton_Click(object sender, EventArgs e)
        {
            GotoEventEditPage(null);
        }

        protected void EventGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("EditEvent"))
            {
                GotoEventEditPage( (int?) Int32.Parse(e.CommandArgument.ToString()));
            }
        }

        private void GotoEventEditPage(int? eventId)
        {
            string pageUli = "/EventPage.aspx";
            if (eventId != null)
                pageUli += "?eventId=" + eventId;
            Response.Redirect(pageUli);
        }

    }
}