﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Model;

namespace WebApp
{
    public partial class EventPage : System.Web.UI.Page
    {
        Repository repository = new Repository();
        // t - edit, f - new
        bool mode;

        protected void Page_PreRender(object sender, EventArgs e)
        {
            dateValidator.MinimumValue = DateTime.Now.Date.ToString("dd.MM.yyyy");
            dateValidator.MaximumValue = DateTime.Now.Date.AddYears(90).ToString("dd.MM.yyyy");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["eventId"] != null)
            {
                mode = true;
                saveButton.Text = "Изменить";

            }
            else
            {
                mode = false;
                saveButton.Text = "Сохранить";
            }
            if (!IsPostBack)
            {
                if (mode)
                {
                    BindData(Int32.Parse(Request.QueryString["eventId"]));
                }
                else
                {
                    BindDataDate(DateTime.Now);

                }

                nameTextBox.Attributes.Add("onblur", "ValidatorOnChange(event);");
                dateTextBox.Attributes.Add("onblur", "ValidatorOnChange(event);");
                timeTextBox.Attributes.Add("onblur", "ValidatorOnChange(event);");
                placeTextBox.Attributes.Add("onblur", "ValidatorOnChange(event);");

            }
            
        }

        private void BindData(int id)
        {
            Event currentEvent = repository.GetEvent(id);

            nameTextBox.Text = currentEvent.Name;

            

            BindDataDate(currentEvent.Date);
            placeTextBox.Text = currentEvent.Place;

            userManager.Users = currentEvent.Users.ToList();

        }

        private void BindDataDate(DateTime datetime)
        {
            nameTextBox.Focus();
            dateTextBox.Text = datetime.ToShortDateString();
            timeTextBox.Text = datetime.ToShortTimeString();
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Event pageEvent = new Event();
                pageEvent.Name = nameTextBox.Text;
                string datetime = dateTextBox.Text + " " + timeTextBox.Text;
                var tmp = DateTime.Parse(datetime);
                pageEvent.Date = tmp;
                pageEvent.Place = placeTextBox.Text;
                pageEvent.Users = userManager.Users;

                if (mode)
                {
                    pageEvent.Id = Int32.Parse(Request.QueryString["eventId"]);
                     repository.UpdateEvent(pageEvent);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "myalert", "alert('Событие изменено!');", true);
                }
                else
                {
                     repository.AddEvent(pageEvent);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "myalert", "alert('Новое событие добавлено!');", true);

                }
            }
        }
    }
}