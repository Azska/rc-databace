﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventPage.aspx.cs" Inherits="WebApp.EventPage" MasterPageFile="~/Site.Master" Title="Редактирование события" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="myControls" Namespace="WebApp.Controls" Assembly="WebApp" %>


<asp:Content runat="server" ContentPlaceHolderID="MainContent">

            <div>
        <table>
            <tr>
                <td>Название:
                    </td>
                <td>
                    <asp:TextBox runat="server" ID="nameTextBox" />
                    <asp:RequiredFieldValidator runat="server" ID="reqNameTextBoxValidator" ControlToValidate="nameTextBox"
                        ForeColor="Red" Text="*" ValidationGroup="validation" />
                    <asp:RegularExpressionValidator runat="server" ID="nameTextBoxValudator" ControlToValidate="nameTextBox"
                        ForeColor="Red" Text="*" ValidationGroup="validation" ValidationExpression="[\S]{4,64}" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">Дата и время:
                    </td>
                <td>
                    <asp:TextBox runat="server" ID="dateTextBox" />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                        TargetControlID="dateTextBox" Format="dd.MM.yyyy" />

                    <asp:RangeValidator ID="dateValidator" runat="server" ControlToValidate="dateTextBox"
                        Type="Date" Text="*" ForeColor="Red" ValidationGroup="validation" />
                    <br />
                    <asp:TextBox runat="server" ID="timeTextBox" />
                    <asp:RegularExpressionValidator ID="timeValidator" runat="server" ControlToValidate="timeTextBox"
                        Text="*" ForeColor="Red" ValidationGroup="validation" ValidationExpression="^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$" />
                </td>
            </tr>
            <tr>
                <td>Место:
                    </td>
                <td>
                    <asp:TextBox runat="server" ID="placeTextBox" />
                    <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ControlToValidate="placeTextBox"
                        ForeColor="Red" Text="*" ValidationExpression="^[\s\S]{0,128}$" Display="Dynamic" ValidationGroup="validation" />

                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">Участники: 
                    </td>
                <td>
                    <myControls:ManageUsersControl runat="server" ID="userManager"  />
                    </td>
            </tr>
        </table>
        <asp:Button runat="server" ID="saveButton" OnClick="SaveButton_Click" CausesValidation="true" ValidationGroup="validation" />

    </div>

</asp:Content>
