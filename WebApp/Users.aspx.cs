﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Model;

namespace WebApp
{
    public partial class Users : System.Web.UI.Page
    {
        public List<User> userList { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            Repository repo = new Repository();
            userList = repo.GetAllUsers();
            usersDataGrid.DataSource = userList.OrderBy(x => x.Name).ToList();
            usersDataGrid.DataBind();
        }
    }
}