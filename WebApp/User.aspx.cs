﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Model;

namespace WebApp
{
    public partial class UserPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User selectedUser = null;
            Repository repo = new Repository();
            if (Request.QueryString["userID"] != null)
                selectedUser = repo.GetUser(Int32.Parse(Request.QueryString["userID"]));
            if (selectedUser != null)
            {
                NameLabel.Text = selectedUser.Name;
                ageLabel.Text = selectedUser.Age.ToString();
                emailLabel.Text = selectedUser.Email;

                eventsGridView.DataSource = getOrderedEvents( selectedUser.Events.ToList());
                eventsGridView.DataBind();
            }
        }

        private List<Event> getOrderedEvents(List<Event> events)
        {
            var tmp1 = events.Where(e => e.isUpcoming).OrderBy(e => e.Date).ToList();
            var tmp2 = events.Where(e => !e.isUpcoming).OrderByDescending(e => e.Date).ToList();

            tmp1.AddRange(tmp2);
            return tmp1;
        }

        protected void eventsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Event bindingEvent = (Event)e.Row.DataItem;
                ((Label)e.Row.FindControl("eventInfoLabel")).Text = bindingEvent.Name + " - " +
                        bindingEvent.Date.ToString("hh:mm dd-MMMM-yyyy");
                if (!bindingEvent.isUpcoming)
                    e.Row.BackColor = System.Drawing.Color.LightGray;
            }
        }

    }
}