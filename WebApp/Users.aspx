﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="WebApp.Users" MasterPageFile="~/Site.Master" Title="Пользователи" %>

<asp:Content runat="server" ID="usersHeadContent" ContentPlaceHolderID="HeadContent">
    <link href="Content/pure-min.css" rel="stylesheet" />
</asp:Content>
<asp:Content runat="server" ID="usersContent" ContentPlaceHolderID="MainContent">
    <div>
        <asp:DataGrid runat="server" ID="usersDataGrid" AutoGenerateColumns="false" CssClass="pure-table">
            <Columns>
                <asp:BoundColumn DataField="Name" HeaderText="Имя" />
                <asp:BoundColumn DataField="Age" HeaderText="Возраст" />
                <asp:BoundColumn DataField="Email" HeaderText="Эл. Почта" />
                <asp:TemplateColumn HeaderText="Кол-во событий">
                    <ItemTemplate>
                        <asp:Literal runat="server" ID="lit" Text='<%# Bind("Events.Count") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Профиль">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" Text="Сслыка" NavigateUrl='<%# "/User.aspx?userID=" + Eval("Id") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</asp:Content>
