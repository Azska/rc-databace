﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Model;

namespace WebApp.Controls
{
    public class ManageUsersControl : Control, IPostBackDataHandler, INamingContainer, IValidator
    {
        private List<User> users = new List<User>();
        public List<User> Users
        {
            get {
                return users;
            }
            set { users = value; }
        }

        private bool isValid = false;

        private List<UserDropDownList> dropDowns = new List<UserDropDownList>();

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.Validators.Add(this);

            Page.RegisterRequiresControlState(this);
            Page.RegisterRequiresPostBack(this);
        }


        protected override object SaveControlState()
        {
            object[] state = new object[4];

            state[0] = base.SaveControlState();
            if (users != null)
            {
                state[1] = users;

            }
            return state;
        }

        protected override void LoadControlState(object savedState)
        {
            if (savedState != null)
            {
                object[] state = (object[])savedState;

                base.LoadControlState(state[0]);
                if (state[1] != null)
                {
                    users = (List<User>)state[1];

                }
            }
        }

        protected override void CreateChildControls()
        {
            Controls.Clear();
            CreateControlHierarchy();
            ClearChildViewState();
        }

        private void CreateControlHierarchy()
        {
            dropDowns.Clear();

            if (users != null)
            {
                foreach (User item in users)
                {
                    //Кнопка "Удалить"
                    LinkButton deleteButton = new LinkButton();

                    Controls.Add(deleteButton);

                    deleteButton.Text = "Удалить";
                    deleteButton.CausesValidation = true;
                    deleteButton.CommandName = users.IndexOf(item).ToString();
                    deleteButton.Command += delegate(object sender, CommandEventArgs e)
                    {
                        int removeItemIndex = Int32.Parse(e.CommandName);
                        users.RemoveAt(removeItemIndex);
                    };


                    UserDropDownList dropDown = new UserDropDownList();
                    dropDown.SelectedUser = item.Id;
                    dropDowns.Add(dropDown);
                    dropDown.ID = String.Format("dropDown{0}", dropDowns.IndexOf(dropDown));

                    dropDown.SelectedValue = item.Id.ToString();
                    Controls.Add(dropDown);

                    //Разделитель
                    if (users.IndexOf(item) < users.Count - 1)
                    {
                        Controls.Add(CreateBr());
                    }
                }
            }

            //Разделитель
            Controls.Add(CreateBr());

            //Кнопка "Добавить"            
            Button addButton = new Button();
            Controls.Add(addButton);

            addButton.ID = "addButton";
            addButton.Text = "Добавить";
            addButton.CausesValidation = true;
            addButton.Click += delegate(object sender, EventArgs e)
            {
                User user = new User();

                users.Add(user);
            };

            Validate();
            Controls.Add(CreateBr());

            Label errorLabel = new Label();
            Controls.Add(errorLabel);
            errorLabel.Text = ErrorMessage;
            errorLabel.ForeColor = System.Drawing.Color.Red;
            errorLabel.Visible = !isValid;


            ChildControlsCreated = true;
        }

        private void requiredValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (users.Count > 0);
        }

        public bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            foreach (var dropDown in dropDowns)
            {
                int index = dropDowns.IndexOf(dropDown);

                int warrantId = (postCollection[dropDown.UniqueID] != String.Empty) ? Int32.Parse(dropDown.SelectedValue) : 1;
                users[index].Id = warrantId;
            }

            return false;
        }

        public void RaisePostDataChangedEvent()
        { }

        protected override void OnPreRender(EventArgs e)
        {
            CreateChildControls();
        }

        private Literal CreateBr()
        {
            Literal literal = new Literal();
            literal.Text = "<br/>";

            return literal;
        }



        public string ErrorMessage
        {
            get
            {
                return "Добавьте хотя бы одного пользователя";
            }
            set
            {
                ;
            }
        }

        public bool IsValid
        {
            get
            {
                return isValid;
            }
            set
            {
                isValid = value;
            }
        }

        public void Validate()
        {
            if (this.users.Count !=0)
            {
                isValid = true;
            }
            else
            {
                isValid = false;
            }
        }
    }
}