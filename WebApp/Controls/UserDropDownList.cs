﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using WebApp.Model;

namespace WebApp.Controls
{
    public class UserDropDownList: DropDownList
    {
        private Repository repository = new Repository();

        public int SelectedUser
        {
            get 
            { 
                return Int32.Parse(SelectedValue);
            }
            set
            {
                if (Items.FindByValue(value.ToString()) != null)
                {
                    SelectedValue = value.ToString();
                }
            }
        }

        public UserDropDownList()
        {
            this.Items.Clear();
            List<User> users = repository.GetAllUsers().OrderBy(x=>x.Name).ToList();

            foreach (User user in users)
            { 
                this.Items.Add ( new ListItem()
                {
                    Text = user.Name,
                    Value = user.Id.ToString()
                });
            }
        }
    }
}