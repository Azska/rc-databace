﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;

using WebApp.Model;
using System.Web.ModelBinding;
using System.Data;

namespace WebApp
{
    public class Repository
    {
        AppContext context;

        public Repository()
        {
            context = new AppContext();
        }

        #region user
        public List<User> GetAllUsers()
        {
            return context.Users.ToList();
        }

        public User GetUser(int id)
        {
            return context.Users.Where(user => user.Id == id).FirstOrDefault();
        }
        #endregion

        #region event

        public List<Event> GetEvents(bool isUpcoming)
        {
            IQueryable<Event> events = context.Events.Where(e => e.Date > DateTime.Now);
            if (isUpcoming)
                return events.OrderBy(x => x.Date).ToList();
            else
                return events.OrderByDescending(x => x.Date).ToList();
        }

        public Event GetEvent(int id)
        {
            return context.Events.Where(e => e.Id == id).FirstOrDefault();
        }

        public void UpdateEvent(Event updatedEvent)
        {
            var dbEvent = context.Events.First(x => x.Id == updatedEvent.Id);

            dbEvent.Name = updatedEvent.Name;
            dbEvent.Place = updatedEvent.Place;
            dbEvent.Date = updatedEvent.Date;

            // removes all user form db event
            dbEvent.Users.Clear();
            // insert users form updated into dbItem
            foreach (User u in updatedEvent.Users)
            { 
                dbEvent.Users.Add ( context.Users.First( x => x.Id == u.Id));
            }
            
            context.SaveChanges();
        }

        public void AddEvent(Event newEvent)
        {
            foreach(User u in newEvent.Users)
                context.Users.Attach(u);
            context.Events.Add(newEvent);
            context.SaveChanges();
        }

        #endregion

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}