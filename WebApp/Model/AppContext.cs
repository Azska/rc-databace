﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using WebApp.Model;

namespace WebApp
{
    class AppContext : DbContext
    {

        public DbSet<Event> Events { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<AppContext>(null);
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().HasKey(x => x.Id).ToTable("User");
            modelBuilder.Entity<User>()
                
                .HasMany(u => u.Events)
                .WithMany(e => e.Users)
                .Map(x =>
                    {
                        x.MapLeftKey("UserId");
                        x.MapRightKey("EventId");
                        x.ToTable("UserEvent");
                    });

            modelBuilder.Entity<Event>().Ignore( e => e.isUpcoming);
            modelBuilder.Entity<Event>().HasKey(x => x.Id).ToTable("Event");


        }
    }
}
