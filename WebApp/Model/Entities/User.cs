﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApp.Model;

namespace WebApp.Model
{
    [Serializable]
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Event> Events { get; private set; }
    }
}
