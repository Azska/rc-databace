﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Model
{
    [Serializable]
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Place { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public bool isUpcoming
        {
            get
            {
                if (this.Date > DateTime.Now)
                    return true;
                return false;
            }
        }

    }
}
