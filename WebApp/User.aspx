﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="WebApp.UserPage" MasterPageFile="~/Site.Master" Title="Пользователь" %>

<asp:Content runat="server" ID="userContent" ContentPlaceHolderID="MainContent">
    <div>
        <table>
            <tr>
                <td>
                     <asp:Label runat="server" ID="NameLabel" />
                </td>
                <td>
                    Возраст: <asp:Label runat="server" ID="ageLabel" /> <br/>
                    Эл. почта<asp:Label runat="server" ID="emailLabel" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    События пользователя: <br />
                    <asp:GridView runat="server" ID="eventsGridView" AutoGenerateColumns="false" OnRowDataBound="eventsGridView_RowDataBound" DataKeyNames="Id" ShowHeader="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="eventInfoLabel"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>  
                </td>
            </tr>
        </table>
    </div>
</asp:Content>