﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="WebApp.Events"  MasterPageFile="~/Site.Master"
     Title ="События" %>
<asp:Content runat="server" ID="eventsHeadContent" ContentPlaceHolderID="HeadContent">
    <link href="Content/pure-min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="eventsContent" runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <asp:Button runat="server" ID="createButton" Text="Создать событие" OnClick="CreateButton_Click"/>
        <asp:GridView runat="server" ID="eventGridView" AutoGenerateColumns="false" OnRowCommand="EventGridView_RowCommand" 
            DataKeyNames="Id" CssClass="pure-table">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton runat="server" CommandName="EditEvent" CommandArgument='<%# Eval("Id") %>' Text="Изменить" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Собитие">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Eval("Name") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Дата" >
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Eval("Date", "{0:HH:mm, dd MMMM yyyy}") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Место">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Eval("Place") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Участники" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Eval("Users.Count") %>'  />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>
    </div>
</asp:Content>
